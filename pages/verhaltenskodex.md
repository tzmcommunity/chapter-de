---
layout: page
title: Verhaltenskodex
author: ''
order: 6
hide: false
tags: []
toc: false
menuid: ''
parentid: ''

---
### **LIEBE MITBÜRGERINNEN UND MITBÜRGER DER WELT**

Wir begrüßen Sie bei der Zeitgeist Bewegung (en: The Zeitgeist Movement, TZM), einer weltweiten Basisorganisation, die sich für Nachhaltigkeit und globale soziale Veränderungen einsetzt. Ihre Teilnahme und die aller anderen ist erforderlich, um am größten sozialen Wandel der Welt mitzuwirken, der jemals in der Geschichte begonnen hat. Das Ziel dieser Bewegung ist es, Licht in eine neue Perspektive zu bringen, einen neuen Gedankengang zu entwickeln und das beste Wissen über ein nachhaltigeres und harmonischeres Leben miteinander und mit der übrigen Natur in Einklang zu bringen.  
 Wenn Sie sich an dieser Bewegung engagieren, beteiligen Sie sich an sozialen Versammlungen, an wissenschaftlicher und technologischer Forschung und Praxis, an lokalen und globalen Gemeinschaftsbemühungen, an Wohltätigkeitsaktionen, an der Rettung der Umwelt und sogar an der persönlichen Entwicklung.  
 Jeder von uns ist persönlich dafür verantwortlich, unsere Grundwerte zu unterstützen, die die Einhaltung eines ethischen Verhaltens erfordern. Wir haben diesen Verhaltenskodex herausgegeben, um unsere langjährige Verpflichtung zu bekräftigen, diese Verantwortung aufrechtzuerhalten und Ihnen und allen, die sich an der Bewegung beteiligen, eine Orientierung zu geben.  
 Auf unserem weiteren Weg wird die Zeitgeist-Bewegung dazu beitragen, dass die Botschaft eines nachhaltigeren, kooperativeren und integrativeren Zusammenhalts in der Gesellschaft Gehör findet. Die Verpflichtung zu Integrität, ehrlichem und ethischem Handeln und die Einhaltung der Gesetze sind entscheidend für unseren weiteren Erfolg.  
   
 Wir danken Ihnen für Ihre Teilnahme.  
 

### **KURZE ZUSAMMENFASSUNG**

  
 **1. Bitte behandeln Sie alle mit Respekt.**  
   
 Beleidigende, herabwürdigende und/oder andere respektlose Äußerungen sind unangemessen und inakzeptabel.  
   
 **2. Bitte beteiligen Sie sich nicht an persönlichen Angriffen/Ad Hominem.**  
   
 Ein Ad Hominem streitet mit der Person und/oder greift sie an, anstatt das Thema anzusprechen, falls es nicht um die Person geht. Versuchen Sie nicht auf solche Angriffe zu reagieren, es sei denn, Sie wollen die Situation höflich aufzeigen und die Diskussion auf das Thema umlenken.  
 Drohungen sind nicht erlaubt, weder gegen Einzelpersonen, Gruppen noch gegen die Gemeinschaft als Ganzes. Drohungen sind respektlos und widersprechen dem Gedankengang des TZM in jeder Hinsicht.  
   
 **3. Respektieren Sie die spezifische Aufgabe der Aktivität.**  
   
 Nehmen Sie mit den richtigen Absichten an einem bestimmten Projekt, einer Aufgabe, einer Versammlung, einem Inhalt oder irgendetwas in der Bewegung teil. Während die Aktivität der Bewegung für alle inklusiv ist, belasten Sie diese Aktivität nicht, lenken Sie sie nicht ab, seien Sie nicht respektlos oder problematisch.  
   
 **4. Erinnern Sie sich an den Verhaltenskodex und halten Sie ihn aufrecht.**  
   
 Es liegt in der Verantwortung jedes Einzelnen und in Ihrer Verantwortung, unseren Verhaltenskodex aufrechtzuerhalten und andere, die innerhalb von TZM teilnehmen, zu informieren, um sich selbst, andere und die Integrität von TZM zu schützen. Dies geschieht nicht von allein und deshalb ist jeder verpflichtet dies zu tun.  
   
 **5. Machen Sie Unterschiede zu Stärken.**  
   
 Konzentrieren Sie sich stattdessen darauf, all die Dinge zu korrigieren, bei denen Sie mit einer anderen Person nicht übereinstimmen, und konzentrieren Sie sich darauf Probleme zu lösen und aus Fehlern zu lernen. Machen Sie die Gemeinschaft größer. Zusammenarbeit statt Konkurrenz. 

### **ETHIKKODEX & UMFELD DER GEMEINSCHAFT**

  
 Das Zeitgeist Movement (TZM) praktiziert Gleichberechtigung, Transparenz und Chancengleichheit für jeden, der einen Beitrag zur Bewegung leisten möchte. TZM hält sich an den Global Human Rights Standard der Vereinten Nationen als Grundlage seiner Kommunikationsethik. Wenn Personen behaupten mit TZM verbunden zu sein, und eine Aktivität mit anderen Personen in Assoziation aufnehmen, müssen sie sich an den Verhaltenskodex halten, wie in diesem Dokument beschrieben. Das Ziel ist es, in einem gemeinschaftlichen Umfeld in Verbindung mit TZM zu interagieren, das die Ausbildung, Gesundheit, Sicherheit und geistige oder körperliche Entwicklung einer Person nicht behindert. Die Nichteinhaltung dieses Verhaltenskodexes, insbesondere in einem gemeinschaftlichen Umfeld oder wenn explizit die Verbindung mit TZM beansprucht wird, kann zum Ausschluss und zur Verweigerung der Verbindung mit TZM sowie gegebenenfalls zu möglichen rechtlichen Schritten führen.

### **WIE WIR UNS GEGENSEITIG BEHANDELN**

  
 Innerhalb des TZM behandeln wir uns gegenseitig mit gleichem Respekt und gleicher Würde. Dies bedeutet, dass alle Personen berechtigt sind, sich an der Bewegung zu beteiligen, während sie sich in einem gemeinschaftlichen Umfeld befinden, das frei von Belästigung, Mobbing und Diskriminierung ist. 

Belästigung, Mobbing und Diskriminierung nehmen viele Formen an, darunter:

**•Unerwünschte Bemerkungen, Gesten oder Körperkontakt   
•Die Zurschaustellung oder Verbreitung anstößiger, herabwürdigender oder sexuell expliziter Bilder oder anderer Materialien  
•Beleidigende oder herabwürdigende Witze oder Kommentare (explizit oder durch Anspielungen)   
•Verbale oder körperliche Misshandlungen oder Drohungen  
•Ärger oder Frustration an andere weitergeben**

  
 Diese Bewegung ist nichthierarchisch und lehnt alle Lösungsvorschläge ab, die Gewalt oder Zwang anwenden, um Frieden und Harmonie zu erreichen. Wenn jemand dies tut, während er eine Verbindung mit dem TZM ist, wird ihm jede Verbindung verweigert.

### **INKLUSIVE SEIN**

  
Wir heißen Menschen aller Hintergründe und Identitäten willkommen und unterstützen sie. Dazu gehören unter anderem Personen jeglicher sexueller Orientierung, Geschlechtsidentität und -ausdruck, Rasse, Ethnizität, Kultur, nationaler Herkunft, sozialer und wirtschaftlicher Klasse, Bildungsniveau, Hautfarbe, Einwanderungsstatus, Geschlecht, Alter, Größe, Familienstand, politische Überzeugung, Religion sowie geistige und körperliche Fähigkeiten, die willkommen sind, einen Beitrag zur Bewegung zu leisten.  
 Für die Teilnahme ist keine Mitgliedschaft, keine Studiengebühr, kein akademischer Hintergrund, keine besonderen Zeugnisse, kein Versprechen oder irgendetwas anderes außer der Zustimmung zu diesem Verhaltenskodex erforderlich.  
 Es ist die Absicht von TZM, die Integrität, den Ruf und die Sicherheit aller Personen, die mit der Bewegung in Verbindung stehen, sowie die eigene zu schützen.  
   
 Jeder kommt aus einem anderen Hintergrund, mit anderen Herausforderungen und unterschiedlichen Geschichten. Wir verstehen, dass Menschen in der Lage sind, Dinge zu sagen und zu tun, die nicht beabsichtigt sind. Trotz dieser Fehler verstehen wir es immer, sie als Teil eines größeren Prozesses einzubeziehen. Wenn Sie oder eine andere Person nicht in der Lage sind, den Verhaltenskodex von TZM zu einem bestimmten Zeitpunkt einzuhalten, wird empfohlen eine Pause einzulegen und zu einem späteren Zeitpunkt zurückzukommen, um sich zu beteiligen. Wenn Sie völlig unfähig sind, innerhalb des Verhaltenskodexes zu handeln, wird empfohlen, sich von der Bewegung zu distanzieren.

### **DIVERSITÄT**

  
Wir können Stärke in der Vielfalt finden. Unterschiedliche Menschen haben unterschiedliche Ansichten zu Themen, und das kann für die Lösung von Problemen oder die Entwicklung neuer Ideen wertvoll sein. Nicht verstehen zu können, warum jemand einen Standpunkt vertritt, bedeutet nicht, dass er falsch liegt. Vergessen Sie nicht, dass wir alle Fehler machen und uns gegenseitige Schuldzuweisungen nicht weiterbringen.  
 Konzentrieren Sie sich stattdessen darauf, Probleme zu lösen, aus Fehlern zu lernen und neue Standpunkte zu verstehen. 

### **GEMEINSCHAFTLICHES UMFELD**

  
 Jeder Versammlungsort, jedes Gemeindezentrum, jede Veranstaltung, jeder Projektraum, jede Website oder andere Medien, soziale Medien, Instant-Messaging-Gruppen und jede Aufgabe des Aktivismus, die mit TZM verbunden ist, wird als gemeinschaftliche Umgebung innerhalb der Bewegung definiert. Von allen mit TZM verbundenen Gemeinschaftsumgebungen wird erwartet, dass sie dem Standard des Verhaltenskodex der Bewegung entsprechen. Es wird auch empfohlen, dass ein Exemplar des Verhaltenskodex für alle in den Gemeinschaftsumgebungen zur Verfügung gestellt wird. Dazu gehört die Gewährleistung des Schutzes, der Integrität, der physischen Sicherheit, der psychischen Gesundheit und der Einbeziehung aller Menschen, die sich in einem gemeinschaftlichen Umfeld von TZM aufhalten.

  
 Von allen Personen, die mit TZM in Verbindung stehen, wird erwartet, dass sie den Verhaltenskodex und alle zusätzlichen Sicherheitsregeln und -praktiken einer Situation in einem gemeinschaftlichen Umfeld befolgen. Von allen wird erwartet, dass sie alle anderen Personen, die in Verbindung mit TZM stehen, an den Verhaltenskodex in der Bewegung sowie an alle anderen zusätzlichen Sicherheitsvorkehrungen erinnern und diese durchsetzen, da die Verantwortung nicht bei eingeschränkten Personen liegt, die sich in einem gemeinschaftlichen Umfeld aufhalten.  
   
 Wenn ein Gemeindeumfeld die Standards des Verhaltenskodexes nicht erfüllt und keine Gegenmaßnahmen ergriffen werden, ist das Gemeindeumfeld abzubrechen und wird nicht mehr mit dem TZM in Verbindung gebracht.

### **QUALITÄTSSTANDARDS VON INHALT & AKTIVISMUS**

  
 Jeder, der an Aktivitäten des TZM teilnimmt, ist willkommen, Inhalte zu erstellen oder sich an unterstützenden Aktivitäten zu beteiligen, mit der Absicht, die bestmögliche Integrität der Bewegung zusammen mit allen Personen, die behaupten mit ihr verbunden zu sein, aufrechtzuerhalten und gleichzeitig dem Verhaltenskodex der Bewegung zuzustimmen. Dieser Standard gilt für alles, in allen Formen, in jeder Sprache oder Geste, die zur Repräsentation von TZM oder von Personen, die mit der Bewegung in Verbindung stehen, gemacht werden.

  
 Wenn Sie Aktivitäten durchführen oder Inhalte innerhalb der Bewegung erstellen und sich mit TZM und allen anderen Personen, die damit in Verbindung stehen, assoziieren, erklären Sie sich mit dem Verhaltenskodex von TZM und der Verantwortung einverstanden, dessen Prinzip zu wahren und andere darüber zu informieren. Während es keine Voraussetzung für die Aufnahme von Aktivitäten innerhalb von TZM gibt, besteht die Anforderung einem Qualitätsstandard zu folgen, der die Bewegung und die beteiligten Personen angemessen repräsentiert. Für jeden, der an TZM teilnehmen und damit beginnen möchte, während er sich der Grundlage seines Aktivismus und seines Inhalts nicht sicher ist, sei auf "Die Zeitgeistbewegung definiert" verwiesen, das weiter unten unter "Links & Ressourcen" zu finden ist.

  
 Einige der primären Aktivitäts- und Inhaltsschwerpunkte des TZM umfassen philosophische, hypothetische und bewährte Praktiken der Nachhaltigkeit, der Wissenschaft, der Technologie, der Menschenrechte, des Umweltschutzes und der Wohltätigkeitsoperationen für eine unterstützende Sache und die Verbesserung der globalen Gesellschaft der gesamten Menschheit und der Natur gleichermaßen. Alles andere, was im Folgenden nicht aufgelistet wurde, wird als Off-Topic oder nicht von starker Relevanz für das TZM betrachtet.

  
 Es gibt einige Bereiche, die nicht nur als Off-Topic, sondern auch als schädlich oder falsch repräsentierend für das TZM und diejenigen, die damit in Verbindung stehen, angesehen werden können. Eine Verfälschung der Tätigkeit oder des Gedankengangs von TZM wird nicht toleriert, ebenso wenig wie irreführende Informationen, die vorgeben im Einklang mit der Bewegung zu stehen. Dies schließt ein, ist aber nicht beschränkt auf: die Rechtfertigung der Anwendung von Gewalt, Zwang, Manipulation, Zerstörung oder Kontrolle gegenüber anderen, um Ziele zu erreichen; die Verwendung oder Rechtfertigung der Verwendung von Hassreden oder Diskriminierung von Menschen und oder anderen Gruppen, politischen Parteien oder Nationen; die Fälschung von Informationen für eine Agenda, sei es zur Unterstützung oder Verunglimpfung von TZM; die Förderung von politischen Parteien oder Kampagnen zur Förderung von TZM; das Handeln oder die Registrierung von TZM als politische Einheit; die Verwendung von falschen Vorstellungen, Annahmen oder Informationen ohne wesentliche Beweise zur Unterstützung einer Behauptung.

  
Sollte irgendeine Person, die mit TZM in Verbindung steht, absichtlich Inhalte oder Aktivitäten mit einem dieser genannten Merkmale anbieten, so wird diese Person von der Bewegung ausgeschlossen und es wird ihr nicht erlaubt, sich weiter an Aktivitäten innerhalb von TZM zu beteiligen. Alle Parteien, die ebenfalls behaupten, keine Verbindung mit TZM zu haben und eines der folgenden Merkmale zur Beschreibung der Motive und Ambitionen der Bewegung verwenden, werden durch eine folgende Erklärung widerlegt und können gerichtlich verfolgt werden.

  
 Wählen Sie Ihre Worte sorgfältig, verhalten Sie sich stets professionell, seien Sie freundlich zu anderen und zögern Sie nicht, um Hilfe zu bitten, wenn Sie Unterstützung benötigen, um etwas für die Bewegung Relevantes zu tun.

### **FINANZIELLE ANGELEGENHEITEN**

  
 TZM arbeitet nicht gewinnorientiert und hat kein Interesse an irgendwelchen finanziellen Gewinnen. Jegliches Geld, das im Namen von TZM gesammelt wird, wird ausschließlich zur Finanzierung oder Rückerstattung einer Sache/eines Gegenstandes für die Tätigkeit der Bewegung verwendet. Es wird erwartet, dass jeder Status einer Non-Profit-Organisation, einer Nichtregierungsorganisation, einer Anleihe, eines Fonds, eines Bankkontos oder einer finanziellen/juristischen Organisationseinheit, die TZM benutzt oder mit TZM verbunden ist, dem folgt. Diese finanziellen/juristischen Organisationseinheiten in Verbindung mit TZM sind davon abgehalten, Gewinne für Personen zu erzielen, unabhängig davon, ob sie mit der Bewegung verbunden oder nicht verbunden sind. Sollte dies geschehen, wird die finanzielle / juristische Organisationseinheit, die mit TZM assoziiert oder nicht assoziiert ist, von der Bewegung ausgeschlossen und darf sich nicht weiter an Aktivitäten innerhalb von TZM beteiligen, wobei ihre Handlungen durch eine folgende Erklärung widerlegt werden und rechtliche Schritte eingeleitet werden können.

### **Haben Sie SPASS!**

Es geht nicht immer darum, das Endziel zu erreichen, sondern die Reise auf dem Weg zu genießen. All die anderen Menschen, denen Sie begegnen, die Dinge, die Sie lernen, und die Schritte und Meilensteine, die Sie erreichen. Lassen Sie Ihre Aktivitäten und Ihre Arbeit für die Zeitgeist-Bewegung nicht nur eine lästige Pflicht oder eine mühsame Aufgabe sein, die erledigt werden muss. Versuchen Sie stattdessen, ein nachhaltigeres, gesünderes und erfülltes Leben zu führen. Seien Sie freundlich zu Ihren Nachbarn und Ihrer Familie, helfen Sie Ihrer örtlichen Gemeinschaft und fördern Sie eine Zukunft für alle ohne Armut und Gewalt, wobei Sie auch das große Ganze sehen.  
   
 Wir freuen uns darauf, Sie und Ihre unterstützenden Beiträge für das TZM und den Rest der Menschheit zu sehen und zu treffen. Seien Sie die Veränderung, die Sie in der Welt sehen möchten!